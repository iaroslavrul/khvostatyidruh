from .models import Dogs, Sex

from django.shortcuts import render

from django.views.generic.base import View


class DogsView(View):
    def get(self, request):
        dogs = Dogs.objects.all()
        context = {'dogs': dogs, }
        return render(request, 'dogs/dogs_list.html', context)

class GenderFilter(View):
    def get(self, request):
        genger = Dogs.objects.first()
# def index(request):
#     dogs = Dogs.objects.all()
#     # sex = Sex.objects.all()
#     # age =
#     context = {'dogs': dogs, }
#     # 'rubrics': sex
#     return render(request, 'dogs/index.html', context)


# def by_sex(request, sex_id):
#     dogs = Dogs.objects.filter(rubric=sex_id)
#     sex = Sex.objects.all()
#     current_sex = Sex.objects.get(pk=sex_id)
#     context = {'dogs': dogs, 'rubrics': sex, 'current_rubric': current_sex}
#     return render(request, 'dogs/by_rubric.html', context)

# def by_age(request, age):
#     dogs = Dogs.objects.filter(age=age)
#     ages = Sex.objects.all()
#     current_age = Rubric.objects.get(pk=age)
#     context = {'dogs' : dogs, 'ages' : ages, 'current_age': current_age}
#     return render(request, 'dogs/by_age.html', context)
