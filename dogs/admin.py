from django.contrib import admin

# Register your models here.

from django.contrib import admin

from .models import Dogs, Sex, Fur, Curator, Sectors


class DogsAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'description', 'age', 'image', 'more_photos_link', 'sectors', 'walking', 'keep', 'sex', 'curator', 'ph',
        'fur')
    list_display_links = ('name',)
    search_fields = ('name',)
    list_editable = ('walking', 'ph',)
    list_filter = ('sectors', 'walking', 'curator', 'ph')


class SexAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_display_links = ('name',)
    search_fields = ('name',)


class FurAdmin(admin.ModelAdmin):
    list_display = ('type',)
    list_display_links = ('type',)
    search_fields = ('type',)


class CuratorAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_display_links = ('name',)
    search_fields = ('name',)


class SectorsAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_display_links = ('name',)
    search_fields = ('name',)


admin.site.register(Dogs, DogsAdmin)
admin.site.register(Sex, SectorsAdmin)
admin.site.register(Fur, FurAdmin)
admin.site.register(Curator, CuratorAdmin)
admin.site.register(Sectors, SectorsAdmin)
