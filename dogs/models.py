from django.db import models

class Dogs(models.Model):
    # Вторичная модель
    name = models.CharField(null=True, max_length=50,
                            verbose_name='Имя собаки', db_index=True)
    description = models.TextField(
        max_length=1000, null=True, verbose_name='О собаке', blank=True)
    age = models.PositiveSmallIntegerField(null=True, db_index=True,
                                           verbose_name="Возраст собаки (к-во месяцев)", help_text="В формате целочисленного числа")
    image = models.ImageField(
        upload_to='media', verbose_name="Ссылка на главное фото")
    more_photos_link = models.SlugField(max_length=200, null=True, verbose_name="Ссылка на все фотографии",
                                        help_text='Ссылка на google drive с остальными фотографиями')
    sex = models.ForeignKey(
        'Sex', null=True, on_delete=models.PROTECT, verbose_name='Пол')
    ph = models.BooleanField(default=False, verbose_name="Есть ли ПХ",
                             help_text='Индификатор того, есть ли потенциальные хозяева, которые интересуются этой собакой')
    walking = models.BooleanField(default=False, verbose_name='Нужно выгулять',
                                  help_text='Индификатор того, что собаку нужно выгулять в ближайшее время. После выгула необходимо снять флажок.')
    curator = models.ForeignKey(
        "Curator", null=True, on_delete=models.PROTECT, verbose_name='Куратор собаки')
    fur = models.ForeignKey(
        'Fur', null=True, on_delete=models.PROTECT, verbose_name='Тип шерсти')
    sectors = models.ForeignKey(
        'Sectors', null=True, on_delete=models.PROTECT, verbose_name='Где живёт')
    keep = models.TextField(null=True, verbose_name='Заметки', blank=True)
    #  blank=True - поле необязательное к заполнению
    # unique = True,
    # db_index - индексирует поле - делает поле более быстрым для поиска

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Собаки'
        verbose_name = 'Собака'
        ordering = ['walking']


class Sex(models.Model):
    # Первичная модель
    name = models.CharField(max_length=20, db_index=True,
                            verbose_name='Название')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Полы'
        verbose_name = 'Пол'


class Fur(models.Model):
    type = models.CharField(max_length=30, db_index=True,
                            verbose_name='Тип шерсти')

    def __str__(self):
        return self.type

    class Meta:
        verbose_name_plural = 'Типы шерсти'
        verbose_name = 'Тип шерсти'


class Curator(models.Model):
    name = models.CharField(max_length=100, db_index=True,
                            verbose_name='Имя и фамилия куратора')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Кураторы'
        verbose_name = 'Куратор'


class Sectors(models.Model):
    name = models.CharField(max_length=50, verbose_name='Название участка')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Где живут'
        verbose_name = 'Где живёт'
