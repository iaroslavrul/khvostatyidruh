from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
# from .views import index
from . import views
# , by_sex
urlpatterns = [
    # path('<int:sex_id>/', by_sex(), name='by_sex'),
    path('', views.DogsView.as_view()),
]

# if settings.DEBUG:
#     urlpatterns += static(settings.MEDIA_URL,
#                           document_root=settings.MEDIA_ROOT)
